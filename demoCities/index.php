<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<h3>財務報表資料庫</h3>
			</div>
			<div class="row">
				<p>
					<a href="reset.php" class="btn btn-success">Reset</a>
					<a href="create.php" class="btn btn-success">Create</a>
					<a href="financialReportRetrive.php" class="btn btn-success">Retrieve</a>
					<a href="update.php" class="btn btn-success">Update</a>
					<a href="delete.php" class="btn btn-success">Delete</a>
				</p>
				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>財務報表名稱</th>
								<th>Crawler 名稱</th>
								<th>報表分類</th>
								<th>報表類型</th>
								<th>起始日期</th>
								<th>最新日期</th>
								<th>欄位集合</th>
								<th>日期區間</th>
								<th>Action</th>
							</tr>
						</thead>
					<tbody>
					<?php
						include_once 'db.php';
						$pdo = Database::connect();
						$sql = 'SELECT * FROM financialReport ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							echo '<td>'. $row['crawler'] . '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td><a class="btn" href="delete.php?id='.$row['id'] .'">Delete</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
				<p></p>
				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>財務報表名稱</th>
								<th>Crawler 名稱</th>
								<th>報表分類</th>
								<th>報表類型</th>
								<th>起始日期</th>
								<th>最新日期</th>
								<th>欄位集合</th>
								<th>日期區間</th>
								<th>Action</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM financialReport ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							echo '<td>'. $row['crawler'] . '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td>'. ' '. '</td>';
							echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
			</div>
			<?php Database::disconnect(); ?>
		</div><!-- /container -->
	</body>
</html>
