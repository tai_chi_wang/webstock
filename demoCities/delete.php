<?php
	require 'db.php';
	$id = null;
	if ( !empty($_GET['id'])){
		$id = $_GET['id'];
	}

	if ( null == $id){
		header("Location: index.php");
		exit;
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "DELETE FROM financialReport where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		//$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		header("Location: index.php");
		exit;
	}
?>

