<?php

include_once('companyList.php');

class CompanyInfoCrawler{

    // return array of DOM nodes
    public function getElementsByClassName(DOMDocument $dom, $className){
        $elements = $dom->getElementsByTagName("*");
        $matched = array();

        foreach($elements as $node){
            if(!$node->hasAttributes())
                continue;

            // for hidding some warning messages
            @$classAttribute = $node->attributes->getNamedItem('class');

            if(!$classAttribute)
                continue;

            $classes = explode(' ', $classAttribute->nodeValue);

            if(in_array($className, $classes))
                $matched[]=$node;
        }
        return $matched;
    }

    public function getList(){
        $list = array();
        $listCrawler = new CompanyListCrawler();
        $listCrawler->main();
        //$list = $listCrawler->getStockIDList();
        $list = $listCrawler->getStockIDNameList();
        return $list;
    }

    public function parseContent($htmlContent){
        $dom = new DOMDocument;
        @$dom->loadHTML($htmlContent);

        $info = new StdClass();

	$info->{'crawler'} = "companyInfoCrawler.php";
        $info->{'dateTime'} = array('2013Q1', '2013Q2', '2013Q3', '2013Q4');
	$info->{'financialReportType'} = "綜合損益表";
	$info->{'reportName'} = "簡明綜合損益表(四季)";
        
        $table_doms = $dom->getElementsByTagName('table');
        foreach ($table_doms as $table_dom){
            $tr_doms = $table_dom->getElementsByTagName('tr');
            if($tr_doms->length > 10){
                // there are two tables with same class hasBorder, we only need the large one
                foreach ($tr_doms as $tr_dom){
                    $result = array();

                    $th_doms = $tr_dom->getElementsByTagName('th');
                    $itemName = trim($th_doms->item(0)->nodeValue);

                    $td_doms = $tr_dom->getElementsByTagName('td');
                    $seasonCount = 4;
                    if($seasonCount == $td_doms->length){
                        for($i = 0; $i < $seasonCount ; ++$i){
                            $result[] = trim($td_doms->item($i)->nodeValue);
                        }
                    }else{
                        //echo "!!! Not 4 seasons information !!!\n";
                    }
                    //var_dump($result);

                    //echo $itemName . "\n";
                    if("" != $itemName){
                        $info->{$itemName} = $result;
                    }
                }
            }
        }
        return $info;
    }

    protected $_curl = null;    // singleton

    public function http($url, $post_params){
        if(is_null($this->_curl)){
            $this->_curl = curl_init();
        }
        $curl = $this->_curl;
        $terms = array();
        // directly stored in key-value pattern for query usage, e.g. http://xxx.query?key1=value1 && key2=value2
        foreach ($post_params as $key => $value){
            $terms[] = urlencode($key) . '=' . urlencode($value);
        }
	curl_setopt($curl, CURLOPT_POSTFIELDS, implode('&', $terms));
	curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie-file');
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_REFERER, 'http://mops.twse.com.tw/mops/web/t163sb15');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$content = curl_exec($curl);
	// return str_replace('charset=MS950', 'charset=Big5-2003', $content);
        // process 亂碼
        preg_match("/<title>(.*)<\/title>/s", $content, $match);
        $content = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' . $content;
        return $content;
    }

    // fixme later to store in utility
    public function writeIntoDB($info){
	// 1. connect database
	include_once('db.php');	
	echo "start writing into DB";
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	//var_dump($info);
	// get financial statements ID
	$sql = "SELECT id FROM financialStatements WHERE name='" . $info['financialReportType'] . "';"; 
	$ids = $pdo->query($sql);
	echo $ids[0];
	exit();
	$addFinanReportSQL = "INSERT INTO financialReport (name, crawler, startDate, latestDate, categoryId, typeId) value($info->{'reportName'}, $info->{'crawler'}, $info->{'dateTime'}->item(0), $info->{'dateTime'}->item(3), 1, 1)";

	$q = $pdo->prepare($addFinanReportSQL);
	$q->execute(array($name, $crawler));

	Database::disconnect();
    }

    public function main(){
        // 1. get input list
        $stockIDList = $this->getList();
        //var_dump($stockIDList);
        foreach($stockIDList as $stockNum => $stockName){
            $post_params = array(
                    "encodeURIComponent" => "1",
                    "step" => "1",
                    "firstin" => "1",
                    "off" => "1",
                    "keyword4" => "",
                    "code1" => "",
                    "TYPEK2" => "",
                    "checkbtn" => "",
                    "queryName" => "co_id",
                    "t05st29_c_ifrs" => "N",
                    "t05st30_c_ifrs" => "N",
                    "TYPEK" => "all",
                    "isnew" => "true",
                    "co_id" => $stockNum,
                    "year" => "102",
                    );

		    $url = 'http://mops.twse.com.tw/mops/web/ajax_t163sb15';
                    // 2. use curl to get HTML files
		    $content = $this->http($url, $post_params);
                    sleep(1);

                    // you can use echo $variable or var_dump($variable) to debug
                    //echo $content;

                    // 3. parse HTML 
                    $info = $this->parseContent($content);
                    var_dump($info);
                    
                    // 4. store as a json file
                    //file_put_contents(realpath(dirname(__FILE__)) . '/info/' . $stockNum . '_' . $stockName . '.json', json_encode($info, JSON_UNESCAPED_UNICODE));
		    // 4. write into database
		    $this->writeIntoDB($info);
        }
    }

};

$crawler = new CompanyInfoCrawler;
$crawler->main();

