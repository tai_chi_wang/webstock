<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<h3>View Elements</h3>
			</div>
			<div class="row">
				<p>
					<a href="update.php" class="btn btn-success">Update</a>
				</p>
				<table class="table table-striped table-bordered">
					<div style="overflow:hidden;">
						<select class="selectpicker">
						<option>損益表</option>
						<option>資產負債表</option>
						<option>股東權益表</option>

						</select>
						<select class="selectpicker" data-container="body">
						<option>綜合損益</option>
						<option>子公司損益</option>
						</select>
						<select class="selectpicker" data-container="body">
						<option>1101</option>
						<option>2412</option>
						</select>

						<select class="selectpicker" data-container="body">
						<option>2011Q1</option>
						<option>2011Q2</option>
						</select>
					</div>
					<tbody>
					<?php
						include 'db.php';
						$pdo = Database::connect();
						$sql = 'SELECT * FROM subject ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
			</div>
			<div class="row">
				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Verb</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM verb ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Preposition</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM preposition ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Quantifier</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM quantifier ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Adjective</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM adjective ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>

				<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Object</th>
							</tr>
						</thead>
					<tbody>
					<?php
						$sql = 'SELECT * FROM object ORDER BY id DESC';
						foreach($pdo->query($sql) as $row){
							echo '<tr>';
							echo '<td>'. $row['name'] . '</td>';
							//echo '<td><a class="btn" href="read.php?id='.$row['id'] .'">Read</a></td>';
							echo '</tr>';
						}
						Database::disconnect();
					?>
					</tbody>
				</table>

			</div>
		</div><!-- /container -->
	</body>
</html>
