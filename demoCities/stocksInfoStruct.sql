use stocks;

drop table if exists financialReport, stockInfo, marketType, industryType, status, FRCategory, FREntryGroups, FRType, dateFormat;

create table financialReport(
	id int(8) NOT NULL auto_increment,
	name varchar(64),
	crawler varchar(64),
	startDate date,
	latestDate date,
	categoryId int(4),
	typeId int(4),
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table FREntryGroups(
	id int(4) NOT NULL,
	entry varchar(64) NOT NULL,
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table FRCategory(
	id int(4) NOT NULL auto_increment,
	name varchar(16),
	PRIMARY	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table FRType(
	id int(4) NOT NULL,
	name varchar(16),
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table stockInfo(
        stockNo int(8) NOT NULL,
        shortName varchar(64),
        fullName varchar(128),
        marketIdx int(4),
        industryIdx int(4),
        statusIdx int(4),
        options varchar(256),
        PRIMARY KEY(stockNo)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table marketType(
        id int(4) NOT NULL,
        name varchar(8) NOT NULL,
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table industryType(
        id int(4) NOT NULL,
        name varchar(32) NOT NULL,
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table status(
        id int(4) NOT NULL,
        name varchar(8) NOT NULL,
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

create table dateFormat(
	id int(4) NOT NULL,
	dateFormat date NOT NULL,
	KEY (id)
)engine=InnoDB DEFAULT CHARSET=utf8;

