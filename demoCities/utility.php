<?php
    class utility{
        public static $_curl = null; 
        // curl_params should be a key-value array at least contains URL & REFERER
        public static function getHttpFile($curl_params, $post_params){
            if(is_null($self->_curl)){
                $self->_curl = curl_init();
            }
            $curl = $self->_curl;
            $terms = array();
            // directly stored in key-value pattern for query usage, e.g. http://xxx.query?key1=value1 && key2=value2
            foreach ($post_params as $key => $value){
                $terms[] = urlencode($key) . '=' . urlencode($value);
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, implode('&', $terms));
            curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie-file');
            curl_setopt($curl, CURLOPT_URL, $curl_params['URL']);
            curl_setopt($curl, CURLOPT_REFERER,$curl_params['REFERER']); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            
            echo $curl_params['URL'] . "\n";
            echo $curl_params['REFERER'] . "\n";

            $content = curl_exec($curl);
            // return str_replace('charset=MS950', 'charset=Big5-2003', $content);
            // process 亂碼
            preg_match("/<title>(.*)<\/title>/s", $content, $match);
            $content = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' . $content;
            return $content;
        }

        // return array of DOM nodes
        public static function getElementsByClassName(DOMDocument $dom, $className){
            $elements = $dom->getElementsByTagName("*");
            $matched = array();

            foreach($elements as $node){
                if(!$node->hasAttributes())
                    continue;

                // for hidding some warning messages
                @$classAttribute = $node->attributes->getNamedItem('class');

                if(!$classAttribute)
                    continue;

                $classes = explode(' ', $classAttribute->nodeValue);

                if(in_array($className, $classes))
                    $matched[]=$node;
            }
            return $matched;
        }
    };
?>
