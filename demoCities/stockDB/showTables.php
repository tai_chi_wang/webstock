<?php
require_once 'db.php';
$pdo = Database::connect();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<h3>財務報表資料庫</h3>
			</div>
			<div class="row">
				<form class="form" action="query.php" method="post">
					<table class="table table-striped table-bordered">
					<thead><tr><th>Query API</th><tr></thead>
					<tbody>
						<tr><td>財務報表</td><td>股票代號</td><td>存取時間</td><td></td><td></td></tr>
						<?php
							$sql = 'SELECT name FROM financialStatements;';

							$tableNames = $pdo->query($sql);
							echo '<tr>';
							echo '<td>';
							echo '<select name="reportType" class="selectpicker">'; 
							foreach ($tableNames as $row){
								echo '<option>' . $row['name'] . '</option>';
							}
							echo '</select></td>';

							$sql = 'SELECT code, symbol FROM profiles;';
							$stockNames = $pdo->query($sql);
							echo '<td>';
							echo '<select name="code" class="selectpicker">'; 
							foreach ($stockNames as $row){
								echo '<option>' . $row['code'] . ' ' . $row['symbol'] . '</option>';
							}
							echo '</select></td>';

							$sql = 'SELECT DISTINCT year FROM dateTime;';
							$dateTimes = $pdo->query($sql);
							echo '<td>';
							echo '<select name="year" class="selectpicker">'; 
							foreach ($dateTimes as $row){
								echo '<option>' . $row['year'] . '</option>';
							}
							echo '</select></td>';

							$sql = 'SELECT DISTINCT quarter FROM dateTime;';
							$dateTimes = $pdo->query($sql);
							echo '<td>';
							echo '<select name="quarter" class="selectpicker">'; 
							foreach ($dateTimes as $row){
								echo '<option> ';
								echo $row['quarter'];
								/*
								if(0 == $row['quarter']){
									echo '年報';
								}else{
									echo '第' . $row['quarter'] . '季季報';
								}*/
								echo '</option>';
							}
					?>
					</select></td>
					</tr>
					<div class="form-actions">
						<button type="submit" class="btn btn-success">Query</button>
					</div>
				</form>


						
				</tbody>
				</table>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>資料表</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$sql = 'SHOW TABLES;';

						$tableNames = $pdo->query($sql);
						foreach($tableNames as $row){
							echo '<tr>';
							$tableName = $row['Tables_in_stocks'];
							echo '<td><a href="showTable.php?tableName=' . $tableName . '">'. $tableName . '</a></td>';
							echo '<td>';
							if($tableName != 'dataLoader'){
								$queryLoader = "SELECT loader FROM dataLoader WHERE dbTableName='" . $tableName . "';";
								$loaders = $pdo->query($queryLoader);
								foreach($loaders as $loader){	//fixme later
									echo '<a href="crawler/' . $loader['loader'] . '">' . $loader['loader'] . '</a>';
								}
							}
							echo '</td>';

							echo '<td><a class="btn" href="update.php?tableName=' . $tableName . '">Update</a></td>';
							echo '<td><a class="btn" href="deleteTable.php?tableName=' . $tableName . '">Delete</a></td>';
							echo '</tr>';
						}
					?>
					</tbody>
					<a class="btn" href="createTable.php">Create</a>
					<a class="btn href=update.php?tableName=all">UpdateAll</a>
				</table>
			</div>
			<?php Database::disconnect(); ?>
		</div><!-- /container -->
	</body>
</html>
