<?php
class Database
{
	private static $dbName = 'stocks';
	private static $dbHost = 'localhost';
	private static $dbUser = 'money';
	private static $dbPasswd = 'free';

	private static $cont = null;

	public function __construct()
	{
		// we use it as static function, so constructor is not allowed
		die('Init function is not allowed');
	}

	public static function connect()
	{	
		// singleton connection
		if(null == self::$cont)
		{
			try
			{
				self::$cont = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=". self::$dbName, 
						self::$dbUser, 
						self::$dbPasswd,
						array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			}
			catch(PDOException $e)
			{
				die($e->getMessage());
			}
		}
		return self::$cont;
	}

	public static function disconnect()
	{
		self::$cont = null;
	}
}
?>

