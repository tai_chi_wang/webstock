<?php
    require 'db.php';
    $pdo = Database::connect();
    if(!empty($_REQUEST)){
        $keys = array_keys($_REQUEST);
        $sql = "INSERT INTO " . $_REQUEST['tableName'] . " SET ";

        for($i=0; $i < count($keys); ++$i){
            if($keys[$i] != 'tableName' && $keys[$i] != 'MANTIS_PROJECT_COOKIE' &&
                $keys[$i] != 'SQLiteManager_currentLangue'){
                // fixme later, use implode
                $value = (!empty($_REQUEST[$keys[$i]]))? $_REQUEST[$keys[$i]]: "''"; 
                $sql = $sql . $keys[$i] . "='" . $value . "',";
            }
        }

        // strange, str_replace does not work
        $sql[strlen($sql)-1]=';';
        echo $sql;
        //exit();

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare($sql);
        $q->execute(array());
    }
    Database::disconnect();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <a href="showTable.php?tableName= <?php echo $_REQUEST['tableName']; ?>">Back</a>
    </body>
</html>
                                            
