<?php

include_once('db.php');

class CTableFixedStruct
{
	// fixme later, make all SQL as an object, hide SQL statements behind
	public static $financialStatements; 
	public static $translation; 
	public static $profiles;
	public static $markets;
	public static $industry;

	public static function create(){
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "use stocks;";

		$financialStatementsSQL = "drop table if exists financialStatementsSQL;" . 
			"create table financialStatements(
				id int(2) NOT NULL,
				name varchar(32) NOT NULL,
				PRIMARY KEY (id)
			)engine=InnoDB DEFAULT CHARSET=utf8;";
		
		$sql = $sql . $financialStatementsSQL;
		echo $sql;

		$q = $pdo->prepare($sql);
		$q->execute(array());
		Database::disconnect();	
	}

	public static function retrieve(){}
	public static function update(){
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "use stocks;";

		$financialStatementsSQL = 
			"INSERT INTO financialStatements VALUES ('1', '資產負債表');" . 
			"INSERT INTO financialStatements VALUES ('2', '綜合損益表');" . 
			"INSERT INTO financialStatements VALUES ('3', '現金流量表');" . 
			"INSERT INTO financialStatements VALUES ('4', '權益變動表');"; 
	
		$sql = $sql . $financialStatementsSQL;

		$q = $pdo->prepare($sql);
		$q->execute(array());

		Database::disconnect();
	}
	public static function deleteTable(){}

}

CTableFixedStruct::create();
CTableFixedStruct::update();

?>
