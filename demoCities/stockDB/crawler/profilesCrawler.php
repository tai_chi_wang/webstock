<?php

require('utility.php');
include_once('../db.php');

class CompanyListCrawler{
    protected $_stockIdName = NULL;

    public function getStockIDList(){
        $idList = array();
        foreach($this->_stockIdName as $id => $name){
            $idList[] = $id;
        }
        return $idList;
    }

    public function getStockIDNameList(){
        $idList = array();
        foreach($this->_stockIdName as $id => $name){
            $idList[$id] = $name;
        }
        return $idList;
    }

    public function getMarketList(){
        $marketList = array(
                "上市",
                "上櫃",
                "興櫃",
                "公開發行"
        );
        return $marketList;
    }
    
    public function getIndustryList(){
        $industryList = array(
            "01" => "水泥工業",
            "02" => "食品工業",
            "03" => "塑膠工業",
            "04" => "紡織纖維",
            "05" => "電機機械",
            "06" => "電器電纜",
            "21" => "化學工業",
            "22" => "生技醫療業",
            "07" => "化學生技醫療",
            "08" => "玻璃陶瓷",
            "09" => "造紙工業",
            "10" => "鋼鐵工業",
            "11" => "橡膠工業",
            "12" => "汽車工業",
            "24" => "半導體業",
            "25" => "電腦及週邊設備業",
            "26" => "光電業",
            "27" => "通信網路業",
            "28" => "電子零組件業",
            "29" => "電子通路業",
            "30" => "資訊服務業",
            "31" => "其他電子業",
            "13" => "電子工業",
            "23" => "油電燃氣業",
            "14" => "建材營造",
            "15" => "航運業",
            "16" => "觀光事業",
            "17" => "金融保險業",
            "18" => "貿易百貨",
            "19" => "綜合企業",
            "20" => "其他",
            "91" => "存託憑證",
        );
        return $industryList;
    }

    public function parseCompanyList($html){
        $docDom = new DOMDocument;
        @$docDom->loadHTML($html);
        
        $companyListInfo = new StdClass();

        $tableDoms = $docDom->getElementsByTagName('table');

        foreach($tableDoms as $tableDom){
            $subTableDoms = $tableDom->getElementsByTagName('table');
            foreach($subTableDoms as $subTableDom){
                // parse fields
                $thDoms = $subTableDom->getElementsByTagName('th');
                $fields = $thDoms->length;
                if($fields >0 ){
                    foreach($thDoms as $thDom){
                    //    echo $thDom->nodeValue . ", ";
                    }
                }
                    
                $trDoms = $subTableDom->getElementsByTagName('tr');
                foreach($trDoms as $trDom){
                    $tdDoms = $trDom->getElementsByTagName('td');
                    $stockID = NULL;
                    $stockName = NULL;
                    if($tdDoms->length > 0){
                        for($i = 0; $i <= $tdDoms->length; $i++){
                            if($tdDoms->item($i)){
                                $ADoms = $tdDoms->item($i)->getElementsByTagName('a');
                                //echo $thDoms->item($i)->nodeValue . "\t\t" . $ADoms->item(0)->nodeValue . "\n";
                                if(0==$i) $stockID = $ADoms->item(0)->nodeValue;
                                if(1==$i) $stockName = $ADoms->item(0)->nodeValue;
                            }
                        }
                        //echo "echo: " . $stockID . "\t" . $stockName . "\n";
                        $this->_stockIdName[$stockID] = $stockName;
                        //var_dump($this->_stockIdName);
                    }
                }
            }
        }
    }

    public function main(){
        $curl_params = array(
                "URL" => "http://mops.twse.com.tw/mops/web/ajax_quickpgm",
                "REFERER" => "http://mops.twse.com.tw/mops/web/t05st30",
                );

        // fixme later, industryList will change according to different markets
        // foreach($marketList as $market){
        $market = "上市";

        $industryList = $this->getIndustryList();
        //var_dump($industryList);
        $this->_stockIdName = array();

        foreach($industryList as $industryCode => $industryName){
            //echo $industryCode . "\n";
            $post_params = array(
                    "encodeURIComponent" => "1", 
                    "firstin" => "true",
                    "step" => "4",
                    "checkbtn" => "1",
                    "queryName" => "co_id",
                    "TYPEK2" => "sii",
                    "code1" => $industryCode,
                    );
            $http = utility::getHttpFile($curl_params, $post_params);
            sleep(1);
            
            $this->parseCompanyList($http);
        }
        var_dump($this->_stockIdName);
	$this->writeIntoDatabase($this->_stockIdName);
    }

    public function writeIntoDatabase($stockIdName){
	$pdo = Database::connect();
	foreach($stockIdName as $key=>$value){
		$sql = "INSERT INTO profiles SET code='" . $key . "', symbol='" . $value . "'";
		$pdo->query($sql);
	}
	Database::disconnect();
    }
};

$crawler = new CompanyListCrawler();
$crawler->main();
