<?php

abstract class TypesOfAnalysis{
	const Fundamental = 0;
	const Technical = 1;
	const Financial = 2;
	const Bargain = 3;
};

abstract class TypesOfDate{
	const Day = 0;
	const Week = 1;
	const Month = 2;
	const Quarter = 3;
	const Year = 4;
};

?>

