<?php

require_once "types.php";
require_once "db.php";

// base class, all data table should inherit
abstract class baseDBDataTable{
	protected function getName(){ return $this->_name;}
	protected function getDateType(){ return $this->_dateType;}	// return TypesOfDate in types.h
	protected function getAnalysisType(){ return $this->_analysisType;}	// return TypesOfAnalysis in types.h	
	
	abstract protected function getFields();
	abstract protected function getKeys();

	abstract protected function update($date);
	abstract protected function show();

	abstract protected function getNearThreeYears($date);
	abstract protected function getNearFourSeasons($date);

	var $_name;
	var $_dateType;
	var $_analysisType;

	var $_connector;

	function __construct(){
		$this->_connector = Database::connect();
	}

	function __destruct(){
		$this->_connector = null;
		Database::disconnect();
	}

	protected function isTableExist(){
		$sql = "SHOW TABLES LIKE '{$this->_name}';";
		$results = $this->_connector->query($sql);
		echo $results->rowCount();
		return (1 == $results->rowCount())? true: false;
	}

	protected function getUpdateDate($date){
		switch($this->_dateType){
			case TypesOfDate::Day:
			case TypesOfDate::Week:
				return $date->format('d');
				break;
			case TypesOfDate::Month:
				return $date->format('m');
				break;
			case TypesOfDate::Year:
				return $date->format('y');
				break;
		}
	}
};

?>
