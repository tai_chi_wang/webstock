<?php

require_once('./include/db.php');
require_once('./include/types.php');
require_once('./include/baseDBDataTable.php');

class TableDayK extends baseDBDataTable{
	
	protected function getFields(){}
	protected function getKeys(){}
	protected function show(){}
	
	// pass DateTime object
	protected function update($date){
		echo $this->getUpdateDate($date);
		//var_dump($date);
	}

	protected function getNearThreeYears($date){}
	protected function getNearFourSeasons($date){}

	function __construct(){
		parent::__construct();
		//$this->_name = "TableDayK";
		$this->_name = "accountingTitles";
		$this->_dateType = TypesOfDate::Day;
		$this->_analysisType = TypesOfAnalysis::Technical;
	}

	function __destruct(){
	}

    public function parseContent($htmlContent, &$info){
	    $docDOM = new DOMDocument;
	    @$docDOM->loadHTML($htmlContent);

	    $tableDOMs = $docDOM->getElementsByTagName('table');
	    foreach($tableDOMs as $tableDOM){
		    $trDOMs = $tableDOM->getElementsByTagName('tr');
		    if($trDOMs->length > 10){
			    // first row
			    $headers = $trDOMs->item(0);
			    var_dump($headers);
			    $thDOMs = $headers->getElementsByTagName('th');
			    if(0 == $thDOMs->length){ // it is not written as th tag
				    $thDOMs = $headers->getElementsByTagName('td');
			    }
			    
			    $dateTimeNames = array();
			    // alternative method: write directly with type
			    $dropFieldCount = 1;
			    for($i=$dropFieldCount; $i < $thDOMs->length; ++$i){// skip first nonsense field
				$dateTimeNames[] = $thDOMs->item($i)->nodeValue;
			    }

			    // other rows
			    for($i=1; $i < $trDOMs->length; ++$i){
				    $content = array();
				    $trDOM = $trDOMs->item($i);
				    $tdDOMs = $trDOM->getElementsByTagName('td');
				    $itemName = $tdDOMs->item(0)->nodeValue;
				    for($j=1; $j < $tdDOMs->length; ++$j){
					    $content[] = trim($tdDOMs->item($j)->nodeValue);
				    }
				    $info->{$itemName} = $content;
			    }
		    }
	    }
    }

    public function writeIntoDB($info){
	$sql = "SELECT dbTableName FROM financialStatements WHERE name='" . $info->{'financialReportType'} . "';"; 
	$rows = $this->_pdo->query($sql);
	$dbTableName = $rows->fetch(PDO::FETCH_COLUMN);
	$dateTimes = $info->{'dateTimeID'};

	for($i=0; $i < count($dateTimes); ++$i){
		$sql = "INSERT INTO " . $dbTableName . " SET ";
		$entries = " `dateTimeID`='" . $dateTimes[$i] . "',";

		foreach($info as $itemName => $datas){
			if(count($datas) == 1){
				if($itemName === 'stock'){
					$entries = $entries . "`" . $itemName . "`='" . $datas . "',";
				}
			}else if(count($datas) === count($dateTimes)){
				// check if itemName exists
				$checkSQL = "SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" . $dbTableName . "' AND column_name='" . $itemName ."')>0, ";
				$checkSQL .= '"SELECT 1", "' . "ALTER TABLE `" . $dbTableName . "` ADD " . "`" . $itemName . '` FLOAT NOT NULL")';
				//echo $checkSQL;
				$rows = $this->_pdo->query($checkSQL);
				$addSQL = $rows->fetch(PDO::FETCH_COLUMN);
				//echo $addSQL;
				$this->_pdo->query($addSQL);
				// add it
				if($itemName != 'dateTimeID'){
					if($datas[$i] != '-' && $datas[$i] != ''){ 
						// remove 30,000 to 30000
						$entries = $entries . "`" . $itemName . "`='" . str_replace(",", "", $datas[$i]) . "',";
					}
				}
			}
		}
		$entries[strlen($entries)-1]=';';
		$sql = $sql . $entries;
		echo $sql;
		$this->_pdo->query($sql);
	}
    }

    public function getDateTimeIDs($year, $type){
	    $dateTimes = array();
	    if($type == 'quarters'){
		//echo 'types are seasons';
		$fieldCounts = 4;
		for($i=1; $i<= $fieldCounts; ++$i){ 
			$sql = "SELECT id FROM `dateTime` WHERE `year`='" . $year . "' AND `quarter`='" . $i . "';";
			//echo $sql;
			$rows = $this->_pdo->query($sql);
			//var_dump($rows);
			foreach($rows as $row){ $dateTimes[]=$row['id'];}
		}
	    }else if($type == 'years'){
		//echo 'types are years';
		$fieldCounts = 3;
		for($i=0; $i< $fieldCounts; --$i){
			$sql = "SELECT id FROM `dateTime` WHERE `year`='" . ($year - $i) . "' AND `quarter`=0;";
			$rows = $this->_pdo->query($sql);
			foreach($rows as $row){ $dateTimes[]=$row['id'];}
		}
	    }
	    return $dateTimes;
    }

    public function preProcess($content){
	    $processed = null;
	    $processed = str_replace("TH","th", $content);
	    $processed = str_replace("TR","tr", $processed);
	    $processed = str_replace("TD","td", $processed);
	    $processed = str_replace("TABLE","table", $processed);

	    return $processed;
    }

    public function postProcess($content){
	    //preg_match('/genpage\/[a-zA-Z0-9]+\"/', $content, $matches);
	    preg_match('/genpage/', $content, $matches);
	    echo $matches[0];
    }

	public function main(){
		//$today = date_default_timezone_get(); 
		//date_default_timezone_set('Asia/Taipei');
		//$today = date('m/d/Y h:i:s a', time());
		$today = new DateTime('now', new DateTimeZone('Asia/Taipei'));
		$this->update($today);
		echo ($this->isTableExist())? "Find table" : "Table not found";
		exit();
        // 1. prepare Crawler parameters
        //$stockIDList = $this->getStockIDs();
        $stockIDList = array("1101");
	//$years = range(1983, date("Y")); 
	//$months = range(1, 12);
	$years = array(1993);
	$months = array(1);

	$parseString = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>				<script language="javascript">
							window.location.replace("genpage/Report199301/199301_F3_1_8_1101.php?STK_NO=1101&myear=1993&mmon=01");
					</script>";';
	echo $parseString ;
	preg_match('/(\"genpage\/(.*?)\")/', $parseString, $matches);
	echo $matches[0];

	exit();

	foreach($years as $year){
		foreach($months as $month){
			foreach($stockIDList as $stock){
				$getParams = "STK_NO={$stock}&myear={$year}&mmon={$month}";
				//echo $getParams. "<br />";
				$curl_param=array(
				    "URL" => "http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY.php",
				    "REFERER" => "http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY.php",
				);
				$post_params = array(
						"STK_NO"=>$stock,
						"myear"=>$year,
						"mmon"=>$month
						);
				$content = utility::getHttpFile($curl_param, $post_params, true);
				sleep(1);
				echo $content;
		    		file_put_contents(realpath(dirname(__FILE__)) . 'trace.txt', $content); 
			}
		}
	}
	$this->postProcess($content);

	exit();

	foreach($years as $year){
		foreach($stockIDList as $stockNum){
		    $post_params = array(
			    "encodeURIComponent" => "1",
			    "step" => "1",
			    "firstin" => "1",
			    "off" => "1",
			    "keyword4" => "",
			    "code1" => "",
			    "TYPEK2" => "",
			    "checkbtn" => "",
			    "queryName" => "co_id",
			    "t05st29_c_ifrs" => "N",
			    "t05st30_c_ifrs" => "N",
			    "TYPEK" => "all",
			    "isnew" => "false",
			    "co_id" => $stockNum,
			    "year" => $year,
			    );

		    // 2. use curl to get HTML files

		    $curl_param=array(
				    //"URL" => "http://mops.twse.com.tw/mops/web/ajax_t163sb15",
				    //"REFERER"=> "http://mops.twse.com.tw/mops/web/t163sb15"
				    //"URL" => "http://mops.twse.com.tw/mops/web/ajax_t163sb16",
				    //"REFERER"=> "http://mops.twse.com.tw/mops/web/t163sb16"
				    "URL" => "http://mops.twse.com.tw/mops/web/ajax_t05st30",
				    "REFERER"=> "http://mops.twse.com.tw/mops/web/t05st30"
				    
			    );

		    $content = utility::getHttpFile($curl_param, $post_params);
		    sleep(1);

		    //var_dump($content);
		    // 3. parse HTML 
		    $content = $this->preProcess($content);
		    $info = new StdClass();
		    $info->{'type'} = "quarters";	// fixme later
		    $this->parseContent($content, $info);
		    $info->{'stock'} = $stockNum; 
		    //$info->{'financialReportType'} = "資產負債表";
		    $info->{'financialReportType'} = "綜合損益表";
		    $info->{'fullFinancialReportName'} = "簡明綜合損益表(四季)";
		    $info->{'dateTimeID'} = $this->getDateTimeIDs($year, $info->{'type'});
		    
		    // 4. persistence, store into json or database
		    //file_put_contents(realpath(dirname(__FILE__)) . '/info/' . $stockNum . '_' . $stockName . '.json', json_encode($info, JSON_UNESCAPED_UNICODE));
		    $this->writeIntoDB($info);
		    //$i++;
		    //if($i>3) exit();
		}
	}
    }

};

$crawler = new TableDayK;
$crawler->main();

