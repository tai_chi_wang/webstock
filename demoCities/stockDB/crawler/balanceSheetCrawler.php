<?php

include_once('../db.php');
include_once('../utility.php');

class Crawler{

    protected $_pdo = null;
    protected $_curl = null;    

    public function connectDB(){
	    $this->_pdo = Database::connect();
    }

    public function disconnectDB(){
	    Database::disconnect();
    }

    public function getStockIDs(){
        $list = array();
	$sql = "SELECT code FROM profiles;";
	$datas = $this->_pdo->query($sql);
	$list = $datas->fetchALL(PDO::FETCH_COLUMN);
        return $list;
    }

    public function getYearList(){
	$list = array();
	$sql = "SELECT DISTINCT year FROM dateTime;";
	$datas = $this->_pdo->query($sql);
	$list = $datas->fetchAll(PDO::FETCH_COLUMN);
	return $list;
    }

    public function getYearListAfterIFRSs(){
	    $list = array("102", "103");
	    return $list;
    }

    public function parseContent($htmlContent){
        $dom = new DOMDocument;
        @$dom->loadHTML($htmlContent);

        $info = new StdClass();
        
        $table_doms = $dom->getElementsByTagName('table');
        foreach ($table_doms as $table_dom){
            $tr_doms = $table_dom->getElementsByTagName('tr');
            if($tr_doms->length > 10){
                // there are two tables with same class hasBorder, we only need the large one
                foreach ($tr_doms as $tr_dom){
                    $result = array();

                    $th_doms = $tr_dom->getElementsByTagName('th');
                    $itemName = trim($th_doms->item(0)->nodeValue);

                    $td_doms = $tr_dom->getElementsByTagName('td');
		
                    if(4 == $td_doms->length){
			$info->{'type'} = 'quarters';
		    }else if(3 == $td_doms->length){
			$info->{'type'} = 'years';
                    }else{
			$info->{'type'} = 'unknown';
                        //echo "!!! Not 4 seasons information !!!\n";
                    }
		    
		    for($i = 0; $i < $td_doms->length; ++$i){
			$result[] = trim($td_doms->item($i)->nodeValue);
		    }

                    if("" != $itemName){
                        $info->{$itemName} = $result;
                    }
                }
            }
        }
        return $info;
    }

    public function writeIntoDB($info){
	$sql = "SELECT dbTableName FROM financialStatements WHERE name='" . $info->{'financialReportType'} . "';"; 
	$rows = $this->_pdo->query($sql);
	$dbTableName = $rows->fetch(PDO::FETCH_COLUMN);
	$dateTimes = $info->{'dateTimes'};

	for($i=0; $i < count($dateTimes); ++$i){
		$sql = "INSERT INTO " . $dbTableName . " SET ";
		$entries = " `dateTimeID`='" . $dateTimes[$i] . "',";

		foreach($info as $itemName => $datas){
			if(count($datas) == 1){
				if($itemName === 'stock'){
					$entries = $entries . "`" . $itemName . "`='" . $datas . "',";
				}
			}else if(count($datas) === count($dateTimes)){
				if($itemName != 'dateTimes'){
					if($datas[$i] != '-' && $datas[$i] != ''){ 
						// remove 30,000 to 30000
						$entries = $entries . "`" . $itemName . "`='" . str_replace(",", "", $datas[$i]) . "',";
						// for security, you may check $itemName == $title as follows
						// $sql = "SELECT title FROM accountingTitles WHERE title='" . $itemName . "';";
						// $rows = $this->_pdo->query($sql);
						// $title = $rows->fetch(PDO::FETCH_COLUMN);
					}
				}
			}
		}
		$entries[strlen($entries)-1]=';';
		$sql = $sql . $entries;
		echo $sql;
		$this->_pdo->query($sql);
	}
    }

    public function getDateTimeIDs($year, $type){
	    $dateTimes = array();
	    if($type == 'quarters'){
		//echo 'types are seasons';
		$fieldCounts = 4;
		for($i=1; $i<= $fieldCounts; ++$i){ 
			$sql = "SELECT id FROM `dateTime` WHERE `year`='" . $year . "' AND `quarter`='" . $i . "';";
			//echo $sql;
			$rows = $this->_pdo->query($sql);
			//var_dump($rows);
			foreach($rows as $row){ $dateTimes[]=$row['id'];}
		}
	    }else if($type == 'years'){
		//echo 'types are years';
		$fieldCounts = 3;
		for($i=0; $i< $fieldCounts; --$i){
			$sql = "SELECT id FROM `dateTime` WHERE `year`='" . ($year - $i) . "' AND `quarter`=0;";
			$rows = $this->_pdo->query($sql);
			foreach($rows as $row){ $dateTimes[]=$row['id'];}
		}
	    }
	    return $dateTimes;
    }

    public function main(){
        // 1. prepare Crawler parameters
        $stockIDList = $this->getStockIDs();
	$years = $this->getYearListAfterIFRSs();
	$i=0;
	foreach($years as $year){
		foreach($stockIDList as $stockNum){
		    $post_params = array(
			    "encodeURIComponent" => "1",
			    "step" => "1",
			    "firstin" => "1",
			    "off" => "1",
			    "keyword4" => "",
			    "code1" => "",
			    "TYPEK2" => "",
			    "checkbtn" => "",
			    "queryName" => "co_id",
			    "t05st29_c_ifrs" => "N",
			    "t05st30_c_ifrs" => "N",
			    "TYPEK" => "all",
			    "isnew" => "false",
			    "co_id" => $stockNum,
			    "year" => $year,
			    );

		    // 2. use curl to get HTML files

		    $curl_param=array(
				    //"URL" => "http://mops.twse.com.tw/mops/web/ajax_t163sb15",
				    //"REFERER"=> "http://mops.twse.com.tw/mops/web/t163sb15"
				    "URL" => "http://mops.twse.com.tw/mops/web/ajax_t163sb16",
				    "REFERER"=> "http://mops.twse.com.tw/mops/web/t163sb16"
				    
			    );

		    $content = utility::getHttpFile($curl_param, $post_params);
		    sleep(1);

		    // 3. parse HTML 
		    $info = $this->parseContent($content);
		    $info->{'stock'} = $stockNum; 
		    $info->{'financialReportType'} = "資產負債表";
		    //$info->{'financialReportType'} = "綜合損益表";
		    //$info->{'fullFinancialReportName'} = "簡明綜合損益表(四季)";
		    $info->{'fullFinancialReportName'} = "簡明資產負債表(四季)";
		    $info->{'type'} = "quarters";	// fixme later
		    $info->{'dateTimes'} = $this->getDateTimeIDs($year, $info->{'type'});
		    
		    // 4. persistence, store into json or database
		    //file_put_contents(realpath(dirname(__FILE__)) . '/info/' . $stockNum . '_' . $stockName . '.json', json_encode($info, JSON_UNESCAPED_UNICODE));
		    $this->writeIntoDB($info);
		}
	}
    }

};

$crawler = new Crawler;
$crawler->connectDB();
$crawler->main();
$crawler->disconnectDB();

