<?php

class dbTableManager{
	function __construct(){
		$this->_tables = array();
	}

	function __destruct(){
	}

	public function update($date){
		foreach($this->_tables as $table){
			$table->update($date);
		}
	}

	public function addTable($tableName){
		$this->_tables[] = $tableName;
	}

	private $tables;
};
