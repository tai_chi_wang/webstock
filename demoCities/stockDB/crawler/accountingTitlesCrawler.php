<?php

include_once('../db.php');

class accountingTitlesCrawler{

    protected $_curl = null;    // singleton
    protected $_pdo = null;    // singleton
    // return array of DOM nodes
    public function getElementsByClassName(DOMDocument $dom, $className){
        $elements = $dom->getElementsByTagName("*");
        $matched = array();

        foreach($elements as $node){
            if(!$node->hasAttributes())
                continue;

            // for hidding some warning messages
            @$classAttribute = $node->attributes->getNamedItem('class');

            if(!$classAttribute)
                continue;

            $classes = explode(' ', $classAttribute->nodeValue);

            if(in_array($className, $classes))
                $matched[]=$node;
        }
        return $matched;
    }

    public function getStockIDs(){
        $list = array();
	$sql = "SELECT code FROM profiles;";
	$datas = $this->_pdo->query($sql);
	foreach($datas as $data){
		$list[]= $data['code'];
	}
        return $list;
    }

    public function parseContent($htmlContent){
        $dom = new DOMDocument;
        @$dom->loadHTML($htmlContent);

        $info = new StdClass();

        $table_doms = $dom->getElementsByTagName('table');
        foreach ($table_doms as $table_dom){
            $tr_doms = $table_dom->getElementsByTagName('tr');
            if($tr_doms->length > 10){
                // there are two tables with same class hasBorder, we only need the large one
                foreach ($tr_doms as $tr_dom){
                    $result = array();

                    $th_doms = $tr_dom->getElementsByTagName('th');
                    $itemName = trim($th_doms->item(0)->nodeValue);

                    $td_doms = $tr_dom->getElementsByTagName('td');
                    $seasonCount = 4;
                    if($seasonCount == $td_doms->length){
                        for($i = 0; $i < $seasonCount ; ++$i){
                            $result[] = trim($td_doms->item($i)->nodeValue);
                        }
                    }else{
                        //echo "!!! Not 4 seasons information !!!\n";
                    }
                    //var_dump($result);

                    //echo $itemName . "\n";
                    if("" != $itemName){
                        $info->{$itemName} = $result;
                    }
                }
            }
        }
        return $info;
    }

    public function connectDB()
    {
	    $this->_pdo = Database::connect();
    }

    public function disconnectDB()
    {
	    Database::disconnect();
    }

    public function http($url, $post_params){
        if(is_null($this->_curl)){
            $this->_curl = curl_init();
        }
        $curl = $this->_curl;
        $terms = array();
        // directly stored in key-value pattern for query usage, e.g. http://xxx.query?key1=value1 && key2=value2
        foreach ($post_params as $key => $value){
            $terms[] = urlencode($key) . '=' . urlencode($value);
        }
	curl_setopt($curl, CURLOPT_POSTFIELDS, implode('&', $terms));
	curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie-file');
	curl_setopt($curl, CURLOPT_URL, $url);
	//curl_setopt($curl, CURLOPT_REFERER, 'http://mops.twse.com.tw/mops/web/t163sb15');
	curl_setopt($curl, CURLOPT_REFERER, 'http://mops.twse.com.tw/mops/web/t163sb16');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$content = curl_exec($curl);
	// return str_replace('charset=MS950', 'charset=Big5-2003', $content);
        // process 亂碼
        preg_match("/<title>(.*)<\/title>/s", $content, $match);
        $content = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' . $content;
        return $content;
    }

    // fixme later to store in utility
    public function writeIntoDB($info){
	echo $info->{'financialReportType'} . "\n";
	$sql = "SELECT id FROM financialStatements WHERE name='" . $info->{'financialReportType'} . "';"; 
	$ids = $this->_pdo->query($sql);
	$financialReportID = 0;
	foreach($ids as $id){
		$financialReportID = $id['id'];
	}
	echo $financialReportID;

	if($info->{'type'} == 'quarters'){
		$fieldCounts = 4;
	}else if($info->{'type'} == 'years'){
		$fieldCounts = 3;
	}

	foreach($info as $itemName => $result){
		echo $itemName . "\n";
		if($fieldCounts == count($result)){
			$sql = "SELECT * FROM accountingTitles WHERE financialStatementsID=" . $financialReportID . " AND title='" . $itemName . "';";
			$existsData = $this->_pdo->query($sql);
			if($existsData->rowCount() == 0){
				$sql = "INSERT INTO accountingTitles SET financialStatementsID=" . $financialReportID . ",title='" . $itemName . "';";
				$this->_pdo->query($sql);
			}
		}
	}
    }
    
    public function getYearList(){
	$list = array();
	$sql = "SELECT DISTINCT year FROM dateTime;";
	$datas = $this->_pdo->query($sql);
	foreach($datas as $data){
		$list[] = $data['year'];
	}
	return $list;
    }

    public function main(){
	$stockIDList = $this->getStockIDs();
	$yearList = $this->getYearList();
	foreach($yearList as $year){
		foreach($stockIDList as $stockNum){
			// 1. prepare parameters
			$reportsParam = new StdClass();
			$post_params = array(
			    "encodeURIComponent" => "1",
			    "step" => "1",
			    "firstin" => "1",
			    "off" => "1",
			    "keyword4" => "",
			    "code1" => "",
			    "TYPEK2" => "",
			    "checkbtn" => "",
			    "queryName" => "co_id",
			    "t05st29_c_ifrs" => "N",
			    "t05st30_c_ifrs" => "N",
			    "TYPEK" => "all",
			    "isnew" => "false",
			    "co_id" => $stockNum,
			    "year" => $year
			    );

			$compresiveIncomeURL = 'http://mops.twse.com.tw/mops/web/ajax_t163sb15';
			$balanceSheetURL = 'http://mops.twse.com.tw/mops/web/ajax_t163sb16';

			$reportsParam->{'url'} = $balanceSheetURL;
			$reportsParam->{'financialReportType'} = "資產負債表";
			$reportsParam->{'type'} = "quarters";

			// 2. use curl to get HTML files
			$content = $this->http($reportsParam->{'url'}, $post_params);
			sleep(1);

			// 3. parse HTML 
			$info = $this->parseContent($content);
			$info->{'financialReportType'} = $reportsParam->{'financialReportType'};
			$info->{'type'} = $reportsParam->{'type'};

			var_dump($info);
			// 4. persistence, store into json or database
			$this->writeIntoDB($info);
			
			//file_put_contents(realpath(dirname(__FILE__)) . $stockNum . '_' . $stockName . '.json', json_encode($info, JSON_UNESCAPED_UNICODE));
		}
	}
    }
};

$crawler = new accountingTitlesCrawler;
$crawler->connectDB();
$crawler->main();
$crawler->disconnectDB();
