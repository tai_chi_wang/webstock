<?php
	require 'db.php';
	$pdo = Database::connect();
	if(!empty($_POST)){
		$tableName = $_POST['reportType'];
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<h3><?php echo $tableName;?></h3>
			</div>
			<div class="row">
				<table class="table table-striped table-bordered">
				<tbody>
				<?php
					if(!empty($_POST)){
						foreach($_POST as $key=>$value){
							if($key != 'reportType'){
								echo '<tr><td>' . $key . "</td><td>" . $value . "</td></tr>";
							}
						}
						
						$sql = "SELECT dbTableName FROM `financialStatements` WHERE `name`='". $tableName . "';";
						$rows = $pdo->query($sql);
						$dbTableName = $rows->fetch(PDO::FETCH_COLUMN);
						
						$quarter = ($_POST['quarter'])? $_POST['quarter']: 0; 
						$sql = "SELECT id FROM `dateTime` WHERE `year`='" . $_POST['year'] . "' AND `quarter`='" . $quarter . "';";
						$rows = $pdo->query($sql);
						$dateTimeID = $rows->fetch(PDO::FETCH_COLUMN);

						$code = substr($_POST['code'], 0, 4);

						// reportType
						$sql = "SELECT * FROM `" . $dbTableName . "` WHERE `stock`='" . $code . 
							"' AND `dateTimeID`='" . $dateTimeID . "';";

						$rows = $pdo->query($sql);
						$row = $rows->fetchAll(PDO::FETCH_ASSOC);
						foreach($row[0] as $key => $value){
							if($key !== 'stock')
								echo '<tr><td>' . $key . '</td><td>' . number_format(round($value), 0, ".", ",") . '</td></tr>';
						}
					}
				?>
				<a class="btn" href="showTables.php">Back</a>
				</tbody>
				<?php Database::disconnect();?>
		</div><!-- /container -->
	</body>
</html>
