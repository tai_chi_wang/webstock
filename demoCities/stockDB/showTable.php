<?php
	require 'db.php';
	$pdo = Database::connect();
	if(!empty($_GET)){
		$tableName = $_GET['tableName'];
	}
	$showColumnsSQL = 'SHOW columns FROM ' . $tableName;
	$columnNames = $pdo->query($showColumnsSQL);
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<h3><?php echo $tableName;?>
			</div>
			<div class="row">
				<table class="table table-striped table-bordered">
				<?php
					echo '<thead><tr>';
					foreach ($columnNames as $row){
						echo '<th>' . $row['Field'] . '</th>';
					}
					echo '<td></td>';
					echo '</tr></thead>';
				?>	
				<tbody>
				<?php
					$datasSQL = 'SELECT * FROM ' . $tableName;
					$datas = $pdo->query($datasSQL);
						$rows = $datas->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $row){
							echo '<tr>';
							foreach ($row as $key => $value){
								$value = ($key !== 'stock')? number_format(round($value), 0, ".", ","): $value;
								echo '<td>' . $value . '</td>';
							}
							echo '<td><a class="btn" href="deleteItem.php?id=' . $row['id'] . '&&tableName=' . $tableName . '">Delete</a></td></tr>';
						}
				?>
				</tbody>
				</table>

				<div class="row">
					<h3>Add new Data</h3>
				</div>
				<form class="form-horizontal" action="addItem.php?tableName=<?php echo $tableName;?>" method="post">
					<?php
						$showColumnsSQL = 'SHOW columns FROM ' . $tableName;
						$columnNames = $pdo->query($showColumnsSQL);

						foreach($columnNames as $fields){
							$field = $fields['Field'];
							if($field != 'id'){
								echo '<div class="control-group">';
								echo '<label class="control-label">' . $field . '</label>';
								echo '<div class="controls">';
								echo '<input name="' . $field . '" type="text" placeholder="' . $field . '" value="">';
								echo '</div>';
								echo '</div>';
							}
						}

					?>
					<div class="form-actions">
						<button type="submit" class="btn btn-success">Create</button>
						<a class="btn" href="showTables.php">Back</a>
					</div>
				</form>

			</div>
			<?php Database::disconnect(); ?>
		</div><!-- /container -->
	</body>
</html>
