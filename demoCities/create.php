<?php
	require 'db.php';

	if(!empty($_POST)){
		// keep track validation errors
		$addError= null;

		// keep track post values
		$name = $_POST['name'];
		$crawler = $_POST['crawler'];

		// validate input
		$valid = true;
		if(empty($name) && empty($crawler)){ 
			$addError = 'Please enter at least one field';
			$valid = false;
		}

		if(!empty($crawler)){
			include_once($crawler);
		}
		// insert data
		if($valid){
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if(!empty($name)){
				$sql = "INSERT INTO financialReport (name, crawler) value(?, ?)";}
			$q = $pdo->prepare($sql);
			$q->execute(array($name, $crawler));

			Database::disconnect();
			header("Location: index.php");
			exit;
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
        	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="span10 offset1">
				<div class="row">
					<h3>新增財務報表</h3>
				</div>
				
				<form class="form-horizontal" action="create.php" method="post">
					<div class="control-group">
						<label class="control-label">財務報表名稱</label>
						<div class="controls">
							<input name="name" type="text" placeholder="Financial Report Name" value="<?php echo !empty($name)? $name: '';?>">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Crawler名稱</label>
						<div class="controls">
							<input name="crawler" type="text" placeholder="Crawler Name" value="<?php echo !empty($crawler)? $crawler: '';?>">
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn btn-success">新增</button>
						<a class="btn" href="index.php">返回</a>
					</div>
				</form>
			</div>
		</div><!-- /container -->
	</body>
</html>
