<?php
class Profiles extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profiles_model');
	}

	public function index()
	{
		$data['profiles'] = $this->profiles_model->get_profiles();
		$data['title'] = 'Company Profiles';

		$this->load->view('templates/header', $data);
		$this->load->view('profiles/index', $data);
		//$this->load->view('templates/footer');
	}

	public function view($code)
	{
		$data['profile'] = $this->profiles_model->get_profiles($code);

		if(empty($data['profile']))
		{
			show_404();
		}

		$data['title'] = $data['profile']['companyName'];

		$this->load->view('templates/header', $data);
		$this->load->view('profile/view', $data);
		$this->load->view('templates/footer');
		// it should be load view here?
	}
}
