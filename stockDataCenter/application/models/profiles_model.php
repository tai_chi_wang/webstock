<?php
class Profiles_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_profiles($code = FALSE)
	{
		if($code === FALSE)
		{
			$query = $this->db->get('profiles');	// table name
			return $query->result_array();
		}

		// active record, abstraction of query codes
		$query = $this->db->get_where('profiles', array('code'=> $code));
		return $query->row_array();
	}
}
