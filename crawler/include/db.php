<?php
// all query related API could be packed here
class Database
{
	private static $dbName = 'dbstock';
	private static $dbHost = 'localhost';
	private static $dbUser = 'money';
	private static $dbPasswd = 'free';

	private static $cont = null;

	public function __construct()
	{
		// we use it as static function, so constructor is not allowed
		die('Init function is not allowed');
	}

	public static function connect()
	{	
		// singleton connection
		if(null == self::$cont)
		{
			try
			{
				self::$cont = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=". self::$dbName, 
						self::$dbUser, 
						self::$dbPasswd,
						array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				//self::$cont->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e)
			{
				die($e->getMessage());
			}
		}
		return self::$cont;
	}

	public static function disconnect()
	{
		self::$cont = null;
	}

	public static function isTableExist($name){
		$sql = "SHOW TABLES LIKE '{$name}';";
		$results = self::connect()->query($sql);
		return (1 == $results->rowCount())? true: false;
	}

	public static function createTable($tableName, $fields, $keys){
		$sql = "CREATE TABLE {$tableName} ( ";

		$fieldStr = null;
		foreach($fields as $field => $type){
			$fieldStr .= "`{$field}` {$type}, "; 
		}
		$sql .= $fieldStr;

		$keysStr = '';
		if(count($keys) > 0){
			$keysStr .= "PRIMARY KEY(";
			$key = implode(",", $keys);
			$keysStr .= $key;
			$keysStr .= ")";
		}else{
			// remove last comma
			$sql[strlen($sql)-2]='';
		}
		$sql .= $keysStr;
		$sql .= " )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$rows = self::connect()->query($sql);
	}

	public static function getTableMaxTime($tableName){
		// common field "stock_date" for history issue
		$sql = "SELECT `stock_date` FROM {$tableName} ORDER BY `stock_date` DESC LIMIT 1";
		$rows = self::connect()->query($sql);
		$list = $rows->fetchALL(PDO::FETCH_COLUMN);
		return $list;
	}

	public static function getTableMinTime($tableName){
		// common field "stock_date" for history issue
		$sql = "SELECT `stock_date` FROM {$tableName} ORDER BY `stock_date` ASC LIMIT 1";
		$rows = self::connect()->query($sql);
		$list = $rows->fetchALL(PDO::FETCH_COLUMN);
		return $list;
	}
	
	public static function updateMgrDate($startDate, $latestDate, $tableName){
		$sql = "UPDATE `dbTables` SET `dataStartDate`=" . '"' . $startDate . '"' . ", `dataLatestDate`=" . '"' . $latestDate . '"' . " WHERE `name`= " . '"' . $tableName . '"' . ";";
		echo $sql;
		return self::connect()->query($sql);
	}

	public static function execSQL($sql){
		return self::connect()->query($sql);
	}
}

date_default_timezone_set('Asia/Taipei');
?>

