<?php 

class utility{
    static function __fgetcsv(&$handle, $length = null, $d = ",", $e = '"') {
        $d = preg_quote($d);
        $e = preg_quote($e);
        $_line = "";
        $eof=false;
        while ($eof != true) {
            $_line .= (empty ($length) ? fgets($handle) : fgets($handle, $length));
            $itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
            if ($itemcnt % 2 == 0){
                $eof = true;
            }
        }

        $_csv_line = preg_replace('/(?: |[ ])?$/', $d, trim($_line));

        $_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';
        preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
        $_csv_data = $_csv_matches[1];
 
        for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
            $_csv_data[$_csv_i] = preg_replace("/^" . $e . "(.*)" . $e . "$/s", "$1", $_csv_data[$_csv_i]);
            $_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
        }
     
        return empty ($_line) ? false : $_csv_data;
    }

    public static $_curl = null;
    	// curl_params should be a key-value array at least contains URL & REFERER
	public static function getHttpFile($curl_params, $post_params){
		if(is_null(self::$_curl)){
			self::$_curl = curl_init();
		}
		$terms = array();

		// directly stored in key-value pattern for query usage, e.g. http://xxx.query?key1=value1 && key2=value2
		foreach ($post_params as $key => $value){
			$terms[] = urlencode($key) . '=' . urlencode($value);
		}
		curl_setopt(self::$_curl, CURLOPT_POSTFIELDS, implode('&', $terms));
		curl_setopt(self::$_curl, CURLOPT_COOKIEFILE, 'cookie-file');
		curl_setopt(self::$_curl, CURLOPT_URL, $curl_params['URL']);
		curl_setopt(self::$_curl, CURLOPT_REFERER,$curl_params['REFERER']); 
		curl_setopt(self::$_curl, CURLOPT_RETURNTRANSFER, true);

		$content = curl_exec(self::$_curl);
		sleep(1);
		// return str_replace('charset=MS950', 'charset=Big5-2003', $content);
		// process 亂碼
		preg_match("/<title>(.*)<\/title>/s", $content, $match);
		$content = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' . $content;
		return $content;
	}
	
	public static function getIndustryList(){
        $industryList = array(
            "01" => "水泥工業",
            "02" => "食品工業",
            "03" => "塑膠工業",
            "04" => "紡織纖維",
            "05" => "電機機械",
            "06" => "電器電纜",
            "21" => "化學工業",
            "22" => "生技醫療業",
            "07" => "化學生技醫療",
            "08" => "玻璃陶瓷",
            "09" => "造紙工業",
            "10" => "鋼鐵工業",
            "11" => "橡膠工業",
            "12" => "汽車工業",
            "24" => "半導體業",
            "25" => "電腦及週邊設備業",
            "26" => "光電業",
            "27" => "通信網路業",
            "28" => "電子零組件業",
            "29" => "電子通路業",
            "30" => "資訊服務業",
            "31" => "其他電子業",
            "13" => "電子工業",
            "23" => "油電燃氣業",
            "14" => "建材營造",
            "15" => "航運業",
            "16" => "觀光事業",
            "17" => "金融保險業",
            "18" => "貿易百貨",
            "19" => "綜合企業",
            "20" => "其他",
            "91" => "存託憑證",
        );
        return $industryList;
    }
}

?>
