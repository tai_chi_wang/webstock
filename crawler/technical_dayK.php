<?php

require_once(__DIR__ . '/include/db.php');
require_once(__DIR__ . '/include/types.php');
require_once(__DIR__ . '/include/utility.php');
require_once(__DIR__ . '/lib/stockData.php');

class TableDayK extends stockData{
	
	// APIs
	public function updateAll($date){
		// update today first
		$today = new DateTime('now', new DateTimeZone('Asia/Taipei'));
		$this->updateTable($today);
	}

	protected function getFields(){}
	protected function getKeys(){}

	// pass DateTime object
	public function updateTable($date, $stock){
		if($this->isNeedToUpdate($date)){
			$param = $this->prepareCrawlerParam($date, $stock);
			$redirectContent = $this->getRedirectHTML($param["stock"], $param["year"], $param["month"]);
			$csvURL = $this->processRedirectURL($redirectContent);

			$curlParams = array("URL" => $csvURL);
			$csvFile = utility::getHttpFile($curlParams, null);
			$csvFile = iconv('BIG5', 'utf-8', $csvFile);

			$data = $this->parseCSV($csvFile);
			$data->{"stock"}=$stock;
			$this->createData($data);
		}else{
			echo "The data is up-to-date\n";
		}
	}

	private function processRedirectURL($content){
		preg_match('/genpage\/.+\?/', $content, $matches);
		$url = str_replace("?", "", $matches[0]);
		$url = "http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY_print.php?genpage=" . $url . "&type=csv";
		return $url;
	}

	protected function createData($data){
		var_dump($data);
		// first pass
		$stock_no="";
		foreach($data as $itemName => $values){
			if(!is_array($values)){
				$stock_no = $values;
			}
		}

		$allSQL="";
		foreach($data as $itemName => $values){
			$entries="";
			if(is_array($values) && $itemName != "fields"){
				$sql = "INSERT INTO `{$this->_name}` SET ";
				$entries = "`" . self::$constFields[0] . "`='{$stock_no}', ";
				if($itemName !== "fields" && is_array($values)){
					for($i=0; $i<count($values); ++$i){
						if(self::$constFields[$i+1] == "stock_date"){
							// the year is in taiwan format: e.g. 103/06/04 to 2014-06-04
							$taiwanTime = str_replace('/', '-', trim($values[$i]));
							$date = new DateTime($taiwanTime);
							$refDate = new DateTime("2000-01-01");
							if($date < $refDate){
								// less than taiwan 100 year, it will auto-add 19xx, e.g. 83->1983, but it's 1994
								$date->modify("+11 year");
								$refDate = new DateTime("1000-01-01");
								if($date < $refDate){
									// taiwan 100 year
									$date->modify("+1900 year");
								}
							}else{
								$date->modify("+1911 year");
							}
							$values[$i]=$date->format("Y-m-d");
						}

						// str_replace to solve 30,000 to 30000
						$entries = $entries . "`" . self::$constFields[$i+1] . "`='" . str_replace(",","", $values[$i]) . "'    ,";
					}
				}
				$entries[strlen($entries)-1]=';';
				$sql = $sql . $entries;
				if(strlen($entries)<50) $sql='';
				$allSQL .= $sql;
			}
			echo "<br /><br /> {$allSQL} <br /><br />";
		}
		Database::execSQL($allSQL);	
	}

	protected function retrieveData($condition){
	}

	protected function updateData($key){
	}

	protected function deleteData($key){
	}

	protected function getNearThreeYears($date){}
	protected function getNearFourSeasons($date){}

	// inherit __construct from stockData, notice: not implement any constructor, or it will overwrite

	function __destruct(){
	}

	function _reset(){
		$this->_name = "TableDayK";
		$this->_dateType = TypesOfDate::Day;
		$this->_analysisType = TypesOfAnalysis::Technical;
	}
	
	private function parseCSV($csv){
		$data = new StdClass();

		// 1. first line of information
		$lines = explode("\n", $csv);
		$columns = str_getcsv($lines[0]);
		$n = count($lines)-1;

		// 2. second line contains fields
		$fields = str_getcsv($lines[1]);
		$data->{'fields'} = $fields;
		
		// 3. remaining datas
		for($i=2; $i<$n; ++$i){
			$values = str_getcsv($lines[$i]);
			$data->{'data_' . ($i-2)} = $values;
			//var_dump($values); 
		}
		return $data;
	}

	private function prepareCrawlerParam($date, $stockID){
		echo "<br /><br />" . $date->format('Y') . "<br /><br />";
		$param = array(
			"stock" => $stockID,
			"year" => $date->format('Y'),
			"month" => $date->format('m')
			);
		return $param;
	}

	private function isNeedToUpdate($date){

		// compare to latest date
		return true;
	}

	public static $constFields=array(
			"stock_no",
			"stock_date",
			"trading_volume",
			"trading_number",
			"opening_price",
			"high_price",
			"low_price",
			"closing_price",
			"price_change",
			"trading_amount"
			);

	public function createTable(){
		$fields = array(
			self::$constFields[0] => "varchar(10)",	// 股票代號
			self::$constFields[1] => "date",	// 日期
			self::$constFields[2] => "int(11)",	// 成交股數
			self::$constFields[3] => "int(11)", 	// 成交金額
			self::$constFields[4] => "float", 	// 開盤價
			self::$constFields[5] => "float",	// 最高價
			self::$constFields[6] => "float",		// 最低價
			self::$constFields[7] => "float",	// 收盤價
			self::$constFields[8] => "float",	// 漲跌價差
			self::$constFields[9] => "int(11)"	// 成交筆數
		);
		$keys = array(
			"stock_no",
			"stock_date"
		);
		Database::createTable($this->_name, $fields, $keys);
	}

	private function getRedirectHTML($stock, $year, $month){
		$curlParams=array(
			"URL" => "http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY.php",
			"REFERER" => "http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY.php",
		);

		$postParams = array(
			"STK_NO"=>$stock,
			"myear"=>$year,
			"mmon"=>$month
		);

		$redirectPage = utility::getHttpFile($curlParams, $postParams);
		return $redirectPage;
	}
};

