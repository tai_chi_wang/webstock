<?php

require_once(__DIR__ . '/include/db.php');
require_once(__DIR__ . '/include/types.php');
require_once(__DIR__ . '/include/utility.php');
require_once(__DIR__ . '/lib/stockData.php');

class TablePERYieldPBR extends stockData{
	
	// APIs
	protected function getFields(){}
	protected function getKeys(){}
	
	public static $constFields=array(
			"stock_no",
			"stock_date",
			"PER",
			"YieldRatio",
			"PBR"
			);

	public function createTable(){
		$fields = array(
			self::$constFields[0] => "varchar(10)",	// 股票代號
			self::$constFields[1] => "date",	// 日期
			self::$constFields[2] => "float",	// 本益比 Price-Earning Ratio
			self::$constFields[3] => "float", 	// 殖利率
			self::$constFields[4] => "float", 	// 股價淨值比 Price-Book Ratio
		);
		$keys = array(
			"stock_no",
			"stock_date"
		);
		Database::createTable($this->_name, $fields, $keys);
	}

	// pass DateTime object
	public function updateTable($date, $stock){
		// update all instead of specific stock
		$html = $this->getHTML($date);
		$processed = $this->preprocessHTML($html);
		$content = $this->parseHTML($processed, $date->format('Y-m-d'));
		$this->createData($content);
	}

        // fixme later, modify access to public for debug
	public function preprocessHTML($content){
		$content = trim($content);
		$startPos = strpos($content, 'START of');
		$endPos = strpos($content, 'end of');
		$processed = substr($content, $startPos, $endPos-$startPos);
                $processed = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head><body>' . $processed;
                $processed = $processed . '</body><html>';
                return $processed;
	}

        public function parseHTML($html, $date){
            $info = array();
            $docDOM = new DOMDocument;
            @$docDOM->loadHTML($html);

            $tableDOMs = $docDOM->getElementsByTagName('table');
            foreach($tableDOMs as $tableDOM){
                $trDOMs = $tableDOM->getElementsByTagName('tr');
                foreach($trDOMs as $trDOM){
                    $tdDOMs = $trDOM->getElementsByTagName('td');
                    if($tdDOMs->length > 2){
                        $data = array();
                        for($i = 0; $i < $tdDOMs->length; ++$i){
                            if(1 != $i){    // field 1 is stock name, and we don't need it
                                $data[] = $tdDOMs->item($i)->nodeValue;
                            }else{
                                $data[] = $date;
                            }
                        }
                        $info[] = $data;
                    }
                }
            }
            return $info;
        }

	protected function createData($data){
	    $sql = "";
            foreach($data as $row => $values){
            	$entries = "INSERT INTO `PERYieldPBR` SET ";
                if(is_array($values)){
                    for($i=0; $i<count($values); ++$i){
                        $entries = $entries . " `" . self::$constFields[$i] . "`='" . trim($values[$i]) . "',";
                    }
		    $entries[strlen($entries)-1]=';';
                }
		$sql = $sql . $entries;
            }
            echo $sql;
	    Database::execSQL($sql);	
	}

	protected function retrieveData($condition){
	}

	protected function updateData($key){
	}

	protected function deleteData($key){
	}

	// inherit __construct from stockData, notice: not implement any constructor, or it will overwrite

	function __destruct(){
	}

	function _reset(){
		$this->_name = "PERYieldPBR";
		$this->_dateType = TypesOfDate::Day;
		$this->_analysisType = TypesOfAnalysis::Fundamental;
	}
	
	public function getHTML($inputDate){
		$curlParams=array(
			"URL" => "http://www.twse.com.tw/ch/trading/exchange/BWIBBU/BWIBBU_d.php",
			"REFERER" => "http://www.twse.com.tw/ch/trading/exchange/BWIBBU/BWIBBU_d.php"
		);

		$date = clone $inputDate;
		$date->modify("-1911 year");
		var_dump($date);
		
		$postParams = array(
			"input_date"=> $date->format('Y/m/d'),
			"select2"=>"ALL",
			"order"=>"STKNO",
			"login_btn"=>""
		);

		$html= utility::getHttpFile($curlParams, $postParams);
                $html = iconv('BIG5-2003', 'utf-8', $html);
		return $html;
	}
};

