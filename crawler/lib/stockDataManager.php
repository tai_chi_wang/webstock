<?php
require_once("../include/db.php");
require_once("../technical_dayK.php");
require_once("../TablePERYieldPBR.php");

class stockDataManager{

    public static function getInstance(){
	if(!isSet(self::$_instance)){
		self::$_instance = new stockDataManager();
        }
        return self::$_instance;
    }

    public function show(){
		foreach($this->_dbTables as $dbTable){
			$dbTable->show();
		}
    }
	
	public function getAllStockList(){
		$sql = "SELECT code FROM profiles";
		$datas = Database::execSQL($sql);
		$rows = $datas->fetchAll(PDO::FETCH_COLUMN);
		return $rows;
	}

    // CRUD
    public function update($date, $stockList){
//		foreach($stockList as $stock){
			foreach($this->_dbTables as $dbTable){
				$dbTable->updateTable($date, $stock);
			}
//		}
		$this->checkTableDateSync();
	
    }

    function getData(){
        foreach($this->_dbTables as $dbTable){
            $dbTable->getData();
        }
    }

    public static $_instance=null;	// singleton
    private $_dbTables;

    function __construct(){
	    echo "stockManager constructor<br />";
	// init members
	$sql = "SELECT * FROM `dbTables`";
	$datas = Database::connect()->query($sql);
	$rows = $datas->fetchAll(PDO::FETCH_ASSOC);

	// _dbTables records all the tables (in fact, sync is the same thing to query from tables
    	$this->_dbTables = array();
	print_r($rows);
	foreach ($rows as $row){
		/*
		if("TableDayK" == $row['name']){
			// it may be used as singleton
			$dbTable = new TableDayK(
				$row['name'],
				$row['dateTypeIdx'],
				$row['anaylsisTypeIdx'],
				$row['dataStartDate'],
				$row['dataLatestDate']);
			$this->_dbTables[] = $dbTable;
		}
		*/
		// table PER, Yield Rate, PBR
		if("PERYieldPBR" == $row['name']){
			// it may be used as singleton
			$dbTable = new TablePERYieldPBR(
				$row['name'],
				$row['dateTypeIdx'],
				$row['anaylsisTypeIdx'],
				$row['dataStartDate'],
				$row['dataLatestDate']);
			$this->_dbTables[] = $dbTable;
		}
	}

	$this->checkTableDateSync();
    }

    private function checkTableDateSync(){
		foreach ($this->_dbTables as $table){
			// a. check table exists
			if (! Database::isTableExist($table->getName())){
				$table->createTable();
			}

			$tableIsDirty = false;
			// b. check start date correct (equal to smallest date in database)
			$tableMinDate = implode("", Database::getTableMinTime($table->getName()));
			$refStartDate = new DateTime("1990-01-01");
			if($tableMinDate < $table->getDataStartDate() 
			   && $refStartDate < $tableMinDate)
			{ // fixme later
				$table->setDataStartDate($tableMinDate);
				$tableIsDirty = true;
			}

			// c. check latest date correct (large than biggest date in database)
			$tableMaxDate = implode("", Database::getTableMaxTime($table->getName()));
			if($tableMaxDate > $table->getDataLatestDate()){ 
				$table->setDataLatestDate($tableMaxDate);
				$tableIsDirty = true;
			}

			if($tableIsDirty){$this->updateTable($table);}
		}
    }

    private function updateTable($table){
	    Database::updateMgrDate($table->getDataStartDate(), $table->getDataLatestDate(), $table->getName());
    }

    function _reset(){

    }
};

?>
