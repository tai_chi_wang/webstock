<?php

require_once __DIR__ . "/../include/types.php";
require_once __DIR__ . "/../include/db.php";

abstract class stockData{
	// table
	abstract protected function createTable();
	abstract public function updateTable($date, $stock);

	// data
	abstract protected function createData($data);
	abstract protected function retrieveData($condition);
	abstract protected function updateData($key);
	abstract protected function deleteData($key);

	//abstract protected function getNearThreeYears($date);
	//abstract protected function getNearFourSeasons($date);
        
	function __destruct(){
		//$this->_connector = null;
                // fixme later, it may not proper to bundle database with stockData, it should bundle with app
		//Database::disconnect();
	}

	public function show(){
		echo $this->_name;
	}

	protected $_name; // database name
	protected $_dateType;
	protected $_analysisType;

	protected $_dataStartDate;
	protected $_dataLatestDate;

	static $_connector;

	function __construct($name, $dateType, $analysisType, $dataStartDate, $dataLatestDate){
		// fixme later, just call abstract function reset
		$this->_name = $name;
		// fixme, map $dataType(e.g. DAY) to enum 
		$this->_dateType = $dateType;
		$this->_analysisType = $analysisType;
		$this->_dataStartDate = $dataStartDate;
		$this->_dataLatestDate = $dataLatestDate;

		echo "dataStock construct <br />";
		echo "name: {$this->_name} <br />";
		echo "dateType: {$this->_dateType} <br />";
		echo "analysisType: {$this->_analysisType} <br />";
		echo "dataStartDate: {$this->_dataStartDate} <br />";
		echo "dateLatestDate: {$this->_dataLatestDate} <br />";
	}

	// getter & setters
	public function getName(){ return $this->_name;}
	public function getDateType(){ return $this->_dateType;}	
	public function getAnalysisType(){ return $this->_analysisType;} 
	public function getDataStartDate(){ return $this->_dataStartDate;}
	public function getDataLatestDate(){ return $this->_dataLatestDate;}

	public function setName($name){ $this->_name = $name;}
	public function setDateType($dateType){  $this->_dateType = $dateType;}	
	public function setAnalysisType($analysisType){  $this->_analysisType = $analysisType;} 
	public function setDataStartDate($dataStartDate){  $this->_dataStartDate = $dataStartDate;}
	public function setDataLatestDate($dataLatestDate){  $this->_dataLatestDate = $dataLatestDate;}

};

?>
