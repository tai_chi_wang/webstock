
<?php
	require_once('../../include/db.php');
	// fixme later, clone to one static array to avoid database query
	$sql = "show columns from tabledayk";
	$rows = Database::execSQL($sql);
	$optionlist = '<select name="optionlist[]">';
	foreach ($rows as $row){
	    $optionlist .= '<option value="' . $row['Field'] . '">' . $row['Field'] . "</option>";
	    //echo $row['field'] . "<br />";
	}
	$optionlist .= "</select>";

	$operations = '<select name="operations[]">' . '<option value="gt"> > </option>'
			. '<option value="eq"> = </option>'
			. '<option value="lt"> < </option>'
			. "</select>";
	$textinput = '<input type="text" name="text_input[]" value="" size="10" maxlength="10">';
	$stockrule = "<td>" . $optionlist . "</td><td>" . $operations . "</td><td>" . $textinput . "</td>";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Hank Wang">
    <link rel="icon" href="../../3rdparty/bootstrap-3.2.0/docs/favicon.ico">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
    $(document).ready(function() {
        $("#add_rule_button").click
        (
            function() {
                var intId = $("#add_rule_div").length + 1;
                var ruleWrapper = $("<tr class=\"rulewrapper\" id=\"rule" + intId + "\"/>");
                //var ruleName = $("<input type=\"text\" class=\"ruleName\" />");
                var ruleName = '<?php echo $stockrule;?>';
                //var ruleType = $("<select class=\"fieldtype\"><option value=\"checkbox\">Checked</option><option value=\"textbox\">Text</option><option value=\"textarea\">Paragraph</option></select>");
                var removeButton = $("<td><input type=\"button\" class=\"remove btn\" value=\"Remove\" /></td>");
                
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                
                ruleWrapper.append(ruleName);
                //ruleWrapper.append(ruleType);
                ruleWrapper.append(removeButton);
                
                $("#add_rule_div").append(ruleWrapper);
            }
        );
    });
    </script>

    <!-- bootstrap core css -->
    <link href="../../3rdparty/bootstrap-3.2.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- just for debugging purposes. don't actually copy these 2 lines! -->
    <!--[if lt ie 9]><script src="../../3rdparty/bootstrap-3.2.0/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../3rdparty/bootstrap-3.2.0/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- ie10 viewport hack for surface/desktop windows 8 bug -->
    <script src="../../3rdparty/bootstrap-3.2.0/docs/assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- html5 shim and respond.js ie8 support of html5 elements and media queries -->
    <!--[if lt ie 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="row"><h3>選取自選股</h3></div>
            <form name="stockcustomrule" method="post" enctype="multipart/form-data" action="../model/dataQuery.php">
		<table class="table table-striped table-bordered">
			<tbody id="add_rule_div">
			</tbody>
			
			<tr>
			    <td><input type="button" id="add_rule_button" name="add_rule_button" class="btn" value="新增規則">
				<input type="submit" name="submit" class="btn btn-success" value="submit" />
			    </td><td></td><td></td><td></td>
			</tr>
		</table>
            </form>
    </div>
</body>
</html>

