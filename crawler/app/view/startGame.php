<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Hank Wang">
    <link rel="icon" href="../../3rdparty/bootstrap-3.2.0/docs/favicon.ico">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- bootstrap core css -->
    <link href="../../3rdparty/bootstrap-3.2.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- just for debugging purposes. don't actually copy these 2 lines! -->
    <!--[if lt ie 9]><script src="../../3rdparty/bootstrap-3.2.0/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../3rdparty/bootstrap-3.2.0/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- ie10 viewport hack for surface/desktop windows 8 bug -->
    <script src="../../3rdparty/bootstrap-3.2.0/docs/assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- html5 shim and respond.js ie8 support of html5 elements and media queries -->
    <!--[if lt ie 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">

</head>
<body>
    <div class="container">
        <div class="row"><h3>開始遊戲</h3></div>
            <form name="gameConfig" method="post" enctype="multipart/form-data" action="../model/setGameConfig.php">
		<table class="table table-striped table-bordered">
			<tr><td>投資起始時間</td><td><input type="text" id="start_time"></td></tr>
			<tr><td>投資結束時間</td><td><input type="text" id="end_time"></td></tr>
			<tr><td>起始資金</td><td><input type="text" id="init_fund"></td></tr>
			<tr><td>記錄名稱</td><td><input type="text" id="record_name"></td></tr>
			<tr><td>遊戲人物</td><td><input type="text" id="gamer_name"></td></tr>
			<tr>
			    <td><input type="button" id="add_rule_button" name="add_rule_button" class="btn" value="新增規則">
				<input type="submit" name="submit" class="btn btn-success" value="submit" />
			    </td><td></td>
			</tr>
		</table>
            </form>
    </div>
</body>
</html>

