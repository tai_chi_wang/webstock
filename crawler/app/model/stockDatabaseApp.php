<?php

require_once('../../lib/stockDataManager.php');

class stockDatabaseApp{
    function show(){
		$this->_manager->show();
    }

    public function updateDataRangeDates($fromDate, $toDate, $stockList){
	    $interval = DateInterval::createFromDateString('1 day');
	    //$interval = DateInterval::createFromDateString('1 month');
	    $days = new DatePeriod($fromDate, $interval, $toDate);
	    foreach($days as $day){
		    echo $day->format("Y-m-d") . "<br />";
		    $this->_manager->update($day, $stockList);
	    }
    }

    public function updateData($date, $stockList){
	    $this->_manager->update($date, $stockList);
    }

    public function getManager(){
	    return $this->_manager;
    }

    function __construct(){
        $this->_manager = stockDataManager::getInstance();
    }

    function __destruct(){
	    Database::disconnect();
    }

    private $_manager;
};

$app = new stockDatabaseApp();
$app->updateDataRangeDates(new DateTime("2012-01-19"), new DateTime('now'), $app->getManager()->getAllStockList());
?>
