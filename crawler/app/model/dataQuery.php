<?php

require_once('../../include/db.php');

// fixme later, write into an interface file, e.g. DataQueryAPI.php
// get _GET data, if fromDate is not provided, use DateTime('-3 year');
if(!empty($_GET["stockNo"]) && !empty($_GET["toDate"])){
	$dataAgent = new stockQueryApp;
	$fromDate = new DateTime('2003-09-01');
	$data = $dataAgent->getData($fromDate, $_GET["toData"]);
	$dataAgent->writeToJson($data, "data.json");
}

if(!empty($_POST)){
	foreach($_POST as $key => $value){
		if(is_array($value)){
			var_dump($value);
		}else{
			$terms[] = urlencode($key) . '=' . urlencode($value);
		}
	}
	$dataAgent = new stockQueryApp;
	echo $dataAgent->getSQL($terms);
}


class stockQueryApp{
    public function getData($fromDate, $toDate, $stock){
	$data = array();
		
	$interval = DateInterval::createFromDateString('1 day');
	$days = new DatePeriod($fromDate, $interval, $toDate);
	foreach($days as $day){
		$sql = "SELECT trading_volume, opening_price, high_price, low_price, closing_price FROM `tabledayk` WHERE `stock_no`='{$stock}' AND `stock_date`='{$day->format('Y-m-d')}';";
		$datas = Database::connect()->query($sql);
		$rows = $datas->fetchAll(PDO::FETCH_ASSOC);
		foreach ($rows as $row){
			$value = array();
			$value[] = $day->format('U') * 1000.0;
			$value[] = $row['opening_price'];
			$value[] = $row['high_price'];
			$value[] = $row['low_price'];
			$value[] = $row['closing_price'];
			$value[] = $row['trading_volume'];
			$data[] = $value;
		}
	}
	return $data;
    }

    public function writeToJson($data, $jsonFile){
	$json = json_encode($data);
	// solve the issue when .getJson error
	$removeQuotesJson = str_replace('"', '', $json);
	file_put_contents($jsonFile, $removeQuotesJson);
    }

    public function getSQL($terms){
	$conditions = implode('&', $terms);
	return $conditions;
    }
};

?>
